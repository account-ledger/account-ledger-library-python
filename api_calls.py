import logging
import os
from logging import Logger, StreamHandler, FileHandler, Formatter
from typing import Optional, Dict, TextIO

import requests
from requests import Response
from dotenv import load_dotenv

# Create a logger
logger: Logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

# Create handlers
c_handler: StreamHandler[TextIO] = logging.StreamHandler()
f_handler: FileHandler = logging.FileHandler('api_calls.log')
c_handler.setLevel(logging.INFO)
f_handler.setLevel(logging.DEBUG)

# Create formatters and add it to handlers
c_format: Formatter = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
f_format: Formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
c_handler.setFormatter(c_format)
f_handler.setFormatter(f_format)

# Add handlers to the logger
logger.addHandler(c_handler)
logger.addHandler(f_handler)


def insert_user(username: str, passcode: str) -> Optional[Dict[str, str]]:
    """
    Sends a POST request to insert a new user.

    This function uses environment variables to get the API endpoint. The environment variables are loaded using
    the `load_dotenv` function from the `dotenv` module. If the environment variables cannot be loaded, the function
    returns None.

    The function sends a POST request to the API endpoint using the `requests.post` function. If the request fails
    due to a `JSONDecodeError`, the function logs the error and returns None. This exception is raised when the
    response body does not contain valid JSON.

    After the POST request, the function checks the status code of the response. If the status code is not 200,
    indicating that the request was not successful, the function returns None.

    :param username: The username of the new user.
    :type username: str
    :param passcode: The passcode for the new user.
    :type passcode: str
    :return: The JSON response from the API endpoint if successful, None otherwise.
    :rtype: Optional[Dict[str, str]]
    """
    if load_dotenv(verbose=True, override=True, interpolate=False):
        api_endpoint: str = (f'{os.getenv("SERVER_ADDRESS")}/{os.getenv("HTTP_API_FOLDER")}/'
                             f'insertUser{os.getenv("FILE_EXTENSION")}')
        try:
            api_response: Response = requests.post(
                url=api_endpoint,
                data=dict(username=username, passcode=passcode))
            if api_response.status_code != 200:
                logger.error(f"POST request failed with status code: {api_response.status_code}")
                return None
            return api_response.json()
        except requests.exceptions.JSONDecodeError as e:
            logger.error(f"Response body does not contain valid JSON: {e.response.text}")
            return None
    else:
        return None
