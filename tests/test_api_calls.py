from unittest.mock import patch

from requests.exceptions import RequestException

from api_calls import insert_user


@patch('account_ledger_lib.api_calls.requests.post')
def test_insert_user_success(mock_post):
    """
    Test the insert_user function when the API call is successful.

    This test case mocks a successful API response and checks that the insert_user function returns the expected result.
    """
    # Mock the API response
    mock_post.return_value.status_code = 200
    mock_post.return_value.json.return_value = {"status": "0"}

    # Call the function with test data
    result = insert_user("testuser", "testpass")

    # Assert that the function returned the expected result
    assert result == {"status": "0"}


@patch('account_ledger_lib.api_calls.requests.post')
def test_insert_user_failure(mock_post):
    """
    Test the insert_user function when the API call fails.

    This test case mocks a failed API response and checks that the insert_user function returns None.
    """
    # Mock the API response
    mock_post.return_value.status_code = 400
    mock_post.return_value.json.return_value = {"status": "1"}

    # Call the function with test data
    result = insert_user("testuser", "testpass")

    # Assert that the function returned the expected result
    assert result is None


@patch('account_ledger_lib.api_calls.requests.post')
def test_insert_user_exception(mock_post):
    """
    Test the insert_user function when a RequestException is raised.

    This test case mocks a RequestException and checks that the insert_user function returns None.
    """
    # Mock a request exception
    mock_post.side_effect = RequestException

    # Call the function with test data
    result = insert_user("testuser", "testpass")

    # Assert that the function returned the expected result
    assert result is None
