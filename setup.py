from setuptools import setup, find_packages

setup(
    name='account_ledger_lib',
    version='0.1',
    packages=find_packages(),
    install_requires=[
        'requests',
    ],
)